function showImage() {
  var firstValue = document.getElementById("myInput").value;
  firstValue = parseInt(firstValue, 10);

  if (firstValue >= 0 && firstValue < 20) {
    document.getElementById("map_img1").style.display = "none";
    document.getElementById("map_img2").style.display = "none";
    document.getElementById("map_img3").style.display = "none";
    document.getElementById("map_img4").style.display = "none";
    document.getElementById("map_img5").style.display = "none";
    document.getElementById("map_img6").style.display = "none";
    document.getElementById("map_img7").style.display = "none";
    document.getElementById("map_img8").style.display = "none";
    document.getElementById("map_img9").style.display = "none";
    document.getElementById("map_img10").style.display = "none";
    document.getElementById("map_img11").style.display = "none";
    document.getElementById("map_img12").style.display = "none";
    document.getElementById("map_img13").style.display = "none";
    document.getElementById("map_img14").style.display = "none";
    document.getElementById("map_img15").style.display = "none";
    document.getElementById("map_img16").style.display = "none";
    document.getElementById("map_img17").style.display = "none";
    document.getElementById("map_img18").style.display = "none";
    document.getElementById("map_img19").style.display = "none";
    document.getElementById("map_img0").style.display = "none";

    document.getElementById("second_map_img1").style.display = "none";
    document.getElementById("second_map_img2").style.display = "none";
    document.getElementById("second_map_img3").style.display = "none";
    document.getElementById("second_map_img4").style.display = "none";
    document.getElementById("second_map_img5").style.display = "none";
    document.getElementById("second_map_img6").style.display = "none";
    document.getElementById("second_map_img7").style.display = "none";
    document.getElementById("second_map_img8").style.display = "none";
    document.getElementById("second_map_img9").style.display = "none";
    document.getElementById("second_map_img10").style.display = "none";
    document.getElementById("second_map_img11").style.display = "none";
    document.getElementById("second_map_img12").style.display = "none";
    document.getElementById("second_map_img13").style.display = "none";
    document.getElementById("second_map_img14").style.display = "none";
    document.getElementById("second_map_img15").style.display = "none";
    document.getElementById("second_map_img16").style.display = "none";
    document.getElementById("second_map_img17").style.display = "none";
    document.getElementById("second_map_img18").style.display = "none";
    document.getElementById("second_map_img19").style.display = "none";
    document.getElementById("second_map_img0").style.display = "none";

    document.getElementById("third_map_img1").style.display = "none";

    switch (firstValue) {
      case 1:
        document.getElementById("map_img1").style.display = "block";
        break;
      case 2:
        document.getElementById("map_img2").style.display = "block";
        break;
      case 3:
        document.getElementById("map_img3").style.display = "block";
        break;
      case 4:
        document.getElementById("map_img4").style.display = "block";
        break;
      case 5:
        document.getElementById("map_img5").style.display = "block";
        break;
      case 6:
        document.getElementById("map_img6").style.display = "block";
        break;
      case 7:
        document.getElementById("map_img7").style.display = "block";
        break;
      case 8:
        document.getElementById("map_img8").style.display = "block";
        break;
      case 9:
        document.getElementById("map_img9").style.display = "block";
        break;
      case 10:
        document.getElementById("map_img10").style.display = "block";
        break;
      case 11:
        document.getElementById("map_img11").style.display = "block";
        break;
      case 12:
        document.getElementById("map_img12").style.display = "block";
        break;
      case 13:
        document.getElementById("map_img13").style.display = "block";
        break;
      case 14:
        document.getElementById("map_img14").style.display = "block";
        break;
      case 15:
        document.getElementById("map_img15").style.display = "block";
        break;
      case 16:
        document.getElementById("map_img16").style.display = "block";
        break;
      case 17:
        document.getElementById("map_img17").style.display = "block";
        break;
      case 18:
        document.getElementById("map_img18").style.display = "block";
        break;
      case 19:
        document.getElementById("map_img19").style.display = "block";
        break;
      case 0:
        document.getElementById("map_img0").style.display = "block";
        break;
    }
  } else if (firstValue >= 20 && firstValue < 400) {
    secondValue = firstValue / 20;
    residue = firstValue % 20;
    secondValue = Math.trunc(secondValue);

    document.getElementById("map_img1").style.display = "none";
    document.getElementById("map_img2").style.display = "none";
    document.getElementById("map_img3").style.display = "none";
    document.getElementById("map_img4").style.display = "none";
    document.getElementById("map_img5").style.display = "none";
    document.getElementById("map_img6").style.display = "none";
    document.getElementById("map_img7").style.display = "none";
    document.getElementById("map_img8").style.display = "none";
    document.getElementById("map_img9").style.display = "none";
    document.getElementById("map_img10").style.display = "none";
    document.getElementById("map_img11").style.display = "none";
    document.getElementById("map_img12").style.display = "none";
    document.getElementById("map_img13").style.display = "none";
    document.getElementById("map_img14").style.display = "none";
    document.getElementById("map_img15").style.display = "none";
    document.getElementById("map_img16").style.display = "none";
    document.getElementById("map_img17").style.display = "none";
    document.getElementById("map_img18").style.display = "none";
    document.getElementById("map_img19").style.display = "none";
    document.getElementById("map_img0").style.display = "none";

    document.getElementById("second_map_img1").style.display = "none";
    document.getElementById("second_map_img2").style.display = "none";
    document.getElementById("second_map_img3").style.display = "none";
    document.getElementById("second_map_img4").style.display = "none";
    document.getElementById("second_map_img5").style.display = "none";
    document.getElementById("second_map_img6").style.display = "none";
    document.getElementById("second_map_img7").style.display = "none";
    document.getElementById("second_map_img8").style.display = "none";
    document.getElementById("second_map_img9").style.display = "none";
    document.getElementById("second_map_img10").style.display = "none";
    document.getElementById("second_map_img11").style.display = "none";
    document.getElementById("second_map_img12").style.display = "none";
    document.getElementById("second_map_img13").style.display = "none";
    document.getElementById("second_map_img14").style.display = "none";
    document.getElementById("second_map_img15").style.display = "none";
    document.getElementById("second_map_img16").style.display = "none";
    document.getElementById("second_map_img17").style.display = "none";
    document.getElementById("second_map_img18").style.display = "none";
    document.getElementById("second_map_img19").style.display = "none";
    document.getElementById("second_map_img0").style.display = "none";

    document.getElementById("third_map_img1").style.display = "none";

    switch (secondValue) {
      case 1:
        document.getElementById("map_img1").style.display = "block";
        break;
      case 2:
        document.getElementById("map_img2").style.display = "block";
        break;
      case 3:
        document.getElementById("map_img3").style.display = "block";
        break;
      case 4:
        document.getElementById("map_img4").style.display = "block";
        break;
      case 5:
        document.getElementById("map_img5").style.display = "block";
        break;
      case 6:
        document.getElementById("map_img6").style.display = "block";
        break;
      case 7:
        document.getElementById("map_img7").style.display = "block";
        break;
      case 8:
        document.getElementById("map_img8").style.display = "block";
        break;
      case 9:
        document.getElementById("map_img9").style.display = "block";
        break;
      case 10:
        document.getElementById("map_img10").style.display = "block";
        break;
      case 11:
        document.getElementById("map_img11").style.display = "block";
        break;
      case 12:
        document.getElementById("map_img12").style.display = "block";
        break;
      case 13:
        document.getElementById("map_img13").style.display = "block";
        break;
      case 14:
        document.getElementById("map_img14").style.display = "block";
        break;
      case 15:
        document.getElementById("map_img15").style.display = "block";
        break;
      case 16:
        document.getElementById("map_img16").style.display = "block";
        break;
      case 17:
        document.getElementById("map_img17").style.display = "block";
        break;
      case 18:
        document.getElementById("map_img18").style.display = "block";
        break;
      case 19:
        document.getElementById("map_img19").style.display = "block";
        break;
      case 0:
        document.getElementById("map_img0").style.display = "block";
        break;
    }

    switch (residue) {
      case 1:
        document.getElementById("second_map_img1").style.display = "block";
        break;
      case 2:
        document.getElementById("second_map_img2").style.display = "block";
        break;
      case 3:
        document.getElementById("second_map_img3").style.display = "block";
        break;
      case 4:
        document.getElementById("second_map_img4").style.display = "block";
        break;
      case 5:
        document.getElementById("second_map_img5").style.display = "block";
        break;
      case 6:
        document.getElementById("second_map_img6").style.display = "block";
        break;
      case 7:
        document.getElementById("second_map_img7").style.display = "block";
        break;
      case 8:
        document.getElementById("second_map_img8").style.display = "block";
        break;
      case 9:
        document.getElementById("second_map_img9").style.display = "block";
        break;
      case 10:
        document.getElementById("second_map_img10").style.display = "block";
        break;
      case 11:
        document.getElementById("second_map_img11").style.display = "block";
        break;
      case 12:
        document.getElementById("second_map_img12").style.display = "block";
        break;
      case 13:
        document.getElementById("second_map_img13").style.display = "block";
        break;
      case 14:
        document.getElementById("second_map_img14").style.display = "block";
        break;
      case 15:
        document.getElementById("second_map_img15").style.display = "block";
        break;
      case 16:
        document.getElementById("second_map_img16").style.display = "block";
        break;
      case 17:
        document.getElementById("second_map_img17").style.display = "block";
        break;
      case 18:
        document.getElementById("second_map_img18").style.display = "block";
        break;
      case 19:
        document.getElementById("second_map_img19").style.display = "block";
        break;
      case 0:
        document.getElementById("second_map_img0").style.display = "block";
        break;
    }
  } else if (firstValue == 400) {
    document.getElementById("map_img1").style.display = "none";
    document.getElementById("map_img2").style.display = "none";
    document.getElementById("map_img3").style.display = "none";
    document.getElementById("map_img4").style.display = "none";
    document.getElementById("map_img5").style.display = "none";
    document.getElementById("map_img6").style.display = "none";
    document.getElementById("map_img7").style.display = "none";
    document.getElementById("map_img8").style.display = "none";
    document.getElementById("map_img9").style.display = "none";
    document.getElementById("map_img10").style.display = "none";
    document.getElementById("map_img11").style.display = "none";
    document.getElementById("map_img12").style.display = "none";
    document.getElementById("map_img13").style.display = "none";
    document.getElementById("map_img14").style.display = "none";
    document.getElementById("map_img15").style.display = "none";
    document.getElementById("map_img16").style.display = "none";
    document.getElementById("map_img17").style.display = "none";
    document.getElementById("map_img18").style.display = "none";
    document.getElementById("map_img19").style.display = "none";
    document.getElementById("map_img0").style.display = "none";

    document.getElementById("second_map_img1").style.display = "none";
    document.getElementById("second_map_img2").style.display = "none";
    document.getElementById("second_map_img3").style.display = "none";
    document.getElementById("second_map_img4").style.display = "none";
    document.getElementById("second_map_img5").style.display = "none";
    document.getElementById("second_map_img6").style.display = "none";
    document.getElementById("second_map_img7").style.display = "none";
    document.getElementById("second_map_img8").style.display = "none";
    document.getElementById("second_map_img9").style.display = "none";
    document.getElementById("second_map_img10").style.display = "none";
    document.getElementById("second_map_img11").style.display = "none";
    document.getElementById("second_map_img12").style.display = "none";
    document.getElementById("second_map_img13").style.display = "none";
    document.getElementById("second_map_img14").style.display = "none";
    document.getElementById("second_map_img15").style.display = "none";
    document.getElementById("second_map_img16").style.display = "none";
    document.getElementById("second_map_img17").style.display = "none";
    document.getElementById("second_map_img18").style.display = "none";
    document.getElementById("second_map_img19").style.display = "none";
    document.getElementById("second_map_img0").style.display = "none";

    document.getElementById("third_map_img1").style.display = "block";
    document.getElementById("second_map_img0").style.display = "block";
    document.getElementById("map_img0").style.display = "block";
  } else {
    alert("Solo números del 0 al 400");
  }
}
